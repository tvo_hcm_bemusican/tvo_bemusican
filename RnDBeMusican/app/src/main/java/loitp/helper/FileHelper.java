package loitp.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;
import java.util.Random;

/**
 * Created by Loitp on 9/18/2016.
 */
public class FileHelper {

    public static final String FOLDER_APP = "loitp";
    public static final String FOLDER_PHOTO = "photos";
    public static final String TMP_PHOTO = "tmp.jpg";


    private String TAG = getClass().getSimpleName();
    private Context mContext;

    public FileHelper(Context context) {
        this.mContext = context;
    }

    public String getFileNameComic(String url) {
        url = url.replace("/", "");
        url = url.replace(".", "");
        url = url.replace(":", "");
        url = url.replace("-", "");
        return url + ".txt";
    }

    /*
    save tring json to sdcard
    ex: writeToFile("module.json", strJson);
     */
    public boolean writeToFile(String folder, String fileName, String body) {
        boolean isCompelete = true;
        FileOutputStream fos = null;
        try {
            String path = GetFolderPath.getFolderPath(mContext);
            if (folder != null) {
                path = path + "/" + folder;
            }
            final File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.d(TAG, "could not create the directories");
                }
            }
            final File myFile = new File(dir, fileName);
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            fos = new FileOutputStream(myFile);
            fos.write(body.getBytes());
            fos.close();
        } catch (IOException e) {
            DebugLog.d(TAG, e.toString());
            isCompelete = false;
        }
        return isCompelete;
    }

    /*
       * read text file from folder
       */
    public String readTxtFromFolder(String folderName, String fileName) {
        String path = GetFolderPath.getFolderPath(mContext) + (folderName == null ? "/" : (folderName + "/")) + fileName;
        DebugLog.d(TAG, "path: " + path);
        File txtFile = new File(path);
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(txtFile));
            String line;
            while ((line = reader.readLine()) != null) {
                text.append(line + '\n');
            }
            reader.close();
        } catch (IOException e) {
            DebugLog.d(TAG, "readTxtFromFolder===" + e.toString());
        }
        return text.toString();
    }

    /*
       * read text file in raw folder
       */
    public String readTxtFromRawFolder(int nameOfRawFile) {
        InputStream inputStream = mContext.getResources().openRawResource(nameOfRawFile);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (Exception e) {
            DebugLog.d(TAG, e.toString());
        }
        return byteArrayOutputStream.toString();
    }


    /*@param link: the url of the website
      * return true if save success
      * return false if save failed
      * */
    public boolean saveHTMLCodeFromURLToSDCard(String link, String folderName, String fileName) {
        boolean state = false;
        InputStream is = null;
        try {
            URL url = new URL(link);
            is = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            br.close();
            is.close();
            DebugLog.d(TAG, "saveHTMLCodeFromURLToSDCard success: " + stringBuilder.toString());
            writeToFile(folderName, fileName, stringBuilder.toString());
            state = true;
        } catch (Exception e) {
            DebugLog.d(TAG, "saveHTMLCodeFromURLToSDCard failed: " + e.toString());
        }
        return state;
    }

    /*
      save bitmap to sdcard
      @param bitmap
      @param subDirName like Quote
       */
    public boolean saveBitmapToSDCard(Bitmap bitmap, String subDirName) {
        String root = GetFolderPath.getFolderPath(mContext);
        File myDir = new File(root + "/" + subDirName);
        if (!myDir.exists())
            myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".png";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            DebugLog.d(TAG, "saveBitmapToSDCard: " + e.toString());
        }
        return false;
    }

    /*
      save bitmap to sdcard
      @param bitmap
      @param subDirName like Quote
      @param fileName
       */
    public boolean saveBitmapToSDCard(Bitmap bitmap, String subDirName, String fileName) {
        String root = GetFolderPath.getFolderPath(mContext);
        File myDir = new File(root + "/" + subDirName);
        if (!myDir.exists())
            myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        File file = new File(myDir, fileName);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            DebugLog.d(TAG, "saveBitmapToSDCard: " + e.toString());
        }
        return false;
    }

    /*
       * get random quote
       */
    public String getRandomeQuote() {
        InputStream in;
        // quote
        String[] arr_Str = null;
        String str = "";
        try {
            in = mContext.getAssets().open("quote.txt");
            byte[] buffer = new byte[in.available()];
            in.read(buffer);
            in.close();
            String chuoi = new String(buffer);
            arr_Str = chuoi.split("###");

            // for (int i = 0; i < arr_Str.length; i++) {
            // // 1 ki tu dau tien cua moi chap
            // String char1;
            // String[] arrChar = arr_Str[i].split(" ");
            // try {
            // char1 = arrChar[1];
            // } catch (Exception e) {
            // char1 = arr_Str[i].substring(0, 1);
            // }
            // }

            // Random r = new Random();
            // int ran = r.nextInt(arr_Str.length);
            int ran = getRandomNumber(arr_Str.length);
            str = arr_Str[ran];

        } catch (Exception e) {
            // DebugLog.d("bug", e.toString());
        }
        return str;
    }

    /*
     * get random number
     */
    public int getRandomNumber(int length) {
        int x = 0;
        Random r = new Random();
        x = r.nextInt(length);
        return x;
    }

    /*copy file in forder to another forder
    * File sourceLocation = new File(pathImg);
      File targetLocation = new File(GetFolderPath.getFolderPath(SettingActivity.this) + "bkg.jpg");
      copyDirectoryOneLocationToAnotherLocation(sourceLocation, targetLocation);
    * */
    public boolean copyDirectoryOneLocationToAnotherLocation(File sourceLocation, File targetLocation) {
        boolean copyOK = true;
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }
            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {
                copyDirectoryOneLocationToAnotherLocation(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {
            try {
                InputStream in = new FileInputStream(sourceLocation);
                OutputStream out = new FileOutputStream(targetLocation);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
            } catch (Exception e) {
                DebugLog.d(TAG, "copyDirectoryOneLocationToAnotherLocation Exception: " + e.toString());
                copyOK = false;
            }
        }
        return copyOK;
    }
}