package loitp.helper;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Vibrator;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;

/**
 * File created on 11/14/2016.
 *
 * @author loitp
 */
public class DeviceHelper {
    private Context mContext;

    public DeviceHelper(Context mContext) {
        this.mContext = mContext;
    }

    public boolean isTablet() {
        return (mContext.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /*
    check device has navigation bar
     */
    public boolean isNavigationBarAvailable() {
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);
        //DebugLog.d(TAG, "isNavigationBarAvailable: " + (!(hasBackKey && hasHomeKey)));
        return (!(hasBackKey && hasHomeKey));
    }

    /*
      get current android version
      @return int
       */
    public int getCurrentAndroidVersion() {
        int thisVersion;
        try {
            PackageInfo pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            thisVersion = pi.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            thisVersion = 1;
        }
        return thisVersion;
    }

    public void setClipboard(String text) {
        if (android.os.Build.VERSION.SDK_INT <
                android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager)
                    mContext
                            .getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard =
                    (android.content.ClipboardManager) mContext
                            .getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip =
                    android.content.ClipData.newPlainText("Copy", text);
            clipboard.setPrimaryClip(clip);
        }
        //MyToast.show(mContext, mContext.getResources().getString(R.string.copied) + "\n" + text, 0);
    }

    public void vibrate(int length) {
        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(length);
    }
}
