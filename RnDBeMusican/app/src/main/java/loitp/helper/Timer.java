package loitp.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * File created on 11/8/2016.
 *
 * @author loitp
 */
public class Timer {
  private String TAG = getClass().getSimpleName();

  public static String convertDateToMilis(String dateString) {
    String mls = "";
    if (dateString != null) {
      SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yyyy");
      Date testDate = null;
      try {
        testDate = sdf.parse(dateString);
        mls = testDate.getTime() + "";
        DebugLog.d("Timer", "testDate.getTime(): " + testDate.getTime());
      } catch (Exception e) {
        DebugLog.d("Timer: ", "convertDateToMilis: " + e.toString());
      }
    }
    return mls;
  }
}
