package loitp.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Random;


/**
 * File created on 11/3/2016.
 *
 * @author loitp
 */
public class UIHelper {
    private String TAG = getClass().getSimpleName();
    private Context context;

    public UIHelper(Context context) {
        this.context = context;
    }


    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    /*
      * settext marquee
      */
    public void setMarquee(TextView tv, String text) {
        tv.setText(text);
        tv.setSelected(true);
        tv.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tv.setSingleLine(true);
    }


    public String convertBitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public void setImageFromString64(ImageView imageView, String s64) {
        byte[] decodedString = Base64.decode(s64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageView.setImageBitmap(decodedByte);
    }

  /*public void setAnimation(View v) {
    Animation mAnimation;
    mAnimation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
    v.startAnimation(mAnimation);
  }*/

    public GradientDrawable createGradientDrawable() {
        int color = getRandomColor();
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(color);
        gd.setCornerRadius(90f);
        gd.setStroke(1, color);
        return gd;
    }

    /*
      get screenshot
       */
    public Bitmap getBitmapFromView(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        Drawable backgroundDrawable = view.getBackground();
        Canvas canvas = new Canvas(bitmap);
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return bitmap;
    }

    public Bitmap getBitmapFromPath(String path) {
        File imgFile = new File(path);
        if (imgFile.exists()) {
            return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return null;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight, int rotate) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public final static int FLIP_VERTICAL = 1;
    public final static int FLIP_HORIZONTAL = 2;

    public Bitmap flipImage(Bitmap src, int type) {
        Matrix matrix = new Matrix();
        if (type == FLIP_VERTICAL) {
            matrix.preScale(1.0f, -1.0f);
        } else if (type == FLIP_HORIZONTAL) {
            matrix.preScale(-1.0f, 1.0f);
        } else {
            return null;
        }
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

  /*
  chat with fanpage Thugiannao
   */
    /*public void chatMessenger(LNotify lNotify, int idView) {
        PackageManager packageManager = context.getPackageManager();
        boolean isFBInstalled = false;
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.orca", 0).versionCode;
            if (versionCode >= 0)
                isFBInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            DebugLog.d(TAG, "packageManager com.facebook.orca: " + e.toString());
        }
        if (!isFBInstalled) {
            lNotify.show(context.getString(R.string.fb_msg_not_installed), Effects.thumbSlider, idView, R.drawable.error);
        } else {
            Uri uri = Uri.parse("fb-messenger://user/");
            uri = ContentUris.withAppendedId(uri, Long.valueOf("947139732073591"));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                lNotify.show(context.getString(R.string.fb_msg_not_opened), Effects.thumbSlider, idView, R.drawable.error);
            }
        }
    }*/
    
  /*
   * send email support
   */
    /*public void sendEmail(LNotify lNotify, int idView) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getResources().getString(R.string.myEmailDev)});
        i.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.mail_subject_support));
        i.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.mail_text_support));
        try {
            context.startActivity(Intent.createChooser(i, context.getString(R.string.send_mail_via)));
        } catch (android.content.ActivityNotFoundException ex) {
            lNotify.show(context.getString(R.string.ko_co_mail_app), Effects.thumbSlider, idView, R.drawable.error);
        }
    }*/
}

