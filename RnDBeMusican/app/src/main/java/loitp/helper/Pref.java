package loitp.helper;

import android.content.Context;
import android.content.SharedPreferences;

import loitp.constant.Const;


/**
 * File created on 11/15/2016.
 *
 * @author loitp
 */
public class Pref {
    private String TAG = getClass().getSimpleName();
    private Context mContext;
    //private Gson gson;

    public Pref(Context c) {
        this.mContext = c;
        //this.gson = new Gson();
    }

    private final String KEY_USER = "key.user";
    private final String NOT_READY_USE_APPLICATION = "not.ready.use.application";
    private final String SHOWED_DIALOG_GUIDE = "showed.dialog.guide";//true da show, false chua tung show
    private final String READ_HIGH_QUALITY = "read.high.quality";//true high, false normal

    //object
    /*public User getUser() {
        SharedPreferences pref = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0);
        return gson.fromJson(pref.getString(KEY_USER, ""), User.class);
    }

    public void setUser(User user) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0).edit();
        editor.putString(KEY_USER, gson.toJson(user));
        editor.apply();
    }*/

    /////////////////////////////////STRING
  /*public String getWhatRULooking4() {
    SharedPreferences pref = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0);
    return pref.getString(WHAT_R_U_LOOKING_FOR, Const.BUSINESS);
  }

  public void setWhatRULooking4(String value) {
    SharedPreferences.Editor editor = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0).edit();
    editor.putString(WHAT_R_U_LOOKING_FOR, value);
    editor.apply();
  }*/

    /////////////////////////////////BOOLEAN

    /*public Boolean getNotReadyUseApplication() {
        SharedPreferences prefs = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0);
        return prefs.getBoolean(NOT_READY_USE_APPLICATION, false);
    }

    public void setNotReadyUseApplication(Boolean value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0).edit();
        editor.putBoolean(NOT_READY_USE_APPLICATION, value);
        editor.apply();
    }*/


    /////////////////////////////////INT
    /*public int getShowChangeLog() {
        SharedPreferences prefs = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0);
        return prefs.getInt(SHOW_CHANGE_LOG, 0);
    }

    public void setShowChangeLog(int value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(Const.PREFERENCES_FILE_NAME, 0).edit();
        editor.putInt(SHOW_CHANGE_LOG, value);
        editor.apply();
    }*/
}