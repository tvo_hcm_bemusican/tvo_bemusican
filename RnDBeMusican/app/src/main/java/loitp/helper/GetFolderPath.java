package loitp.helper;

import java.io.File;

import android.content.Context;
import android.os.Environment;

public class GetFolderPath {

  static String folderPath;

  public static String getFolderPath(Context context) {
    if (isSdPresent() == true) {
      try {
        File sdPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Magical Photo");
        if (!sdPath.exists()) {
          sdPath.mkdirs();
          folderPath = sdPath.getAbsolutePath();
        } else if (sdPath.exists()) {
          folderPath = sdPath.getAbsolutePath();
        }
      } catch (Exception e) {
      }
      folderPath = Environment.getExternalStorageDirectory().getPath() + "/Magical Photo/";
    } else {
      try {
        File cacheDir = new File(context.getCacheDir(), "Magical Photo/");
        if (!cacheDir.exists()) {
          cacheDir.mkdirs();
          folderPath = cacheDir.getAbsolutePath();
        } else if (cacheDir.exists()) {
          folderPath = cacheDir.getAbsolutePath();
        }
      } catch (Exception e) {
      }
    }
    return folderPath;
  }

  public static boolean isSdPresent() {
    return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
  }

  /*public static String getPathOfFileNameMainComicsListHTMLCode(Context context) {
    return getFolderPath(context) + "/" + FileHelper.FOLDER_APP + "/" + FileHelper.FILE_NAME_MAIN_COMICS_LIST_HTML_CODE;
  }*/
}