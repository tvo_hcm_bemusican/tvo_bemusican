package loitp.helper;

import android.util.Log;

import loitp.constant.Const;

public class DebugLog {
    public static void d(String tag, String msg) {
        if (Const.IS_DEBUG) {
            Log.d(tag, msg);
        }
    }
}