package loitp.helper;

/**
 * File created on 11/4/2016.
 *
 * @author loitp
 */
public class ImageDownloader {

  /*public interface OnImageDownloaderListener {
    public void OnDownloadStarted();

    public void OnDownloadFinished();
  }

  private static final String TAG = "ImageDownloader";
  public static final String DIRECTORY_NAME = "loitp";

  private static ImageDownloader instance = null;
  private ImageLoader imageLoader;

  OnImageDownloaderListener listener = new OnImageDownloaderListener() {
    @Override
    public void OnDownloadStarted() {

    }

    @Override
    public void OnDownloadFinished() {

    }
  };

  protected ImageDownloader(Context context) {
    configImageDownloader(context);
  }

  public static ImageDownloader getInstance(Context context) {
    if (instance == null) {
      instance = new ImageDownloader(context);
    }
    return instance;
  }

  private void configImageDownloader(Context context) {
    //File cacheDir = StorageUtils.getOwnCacheDirectory(context, DIRECTORY_NAME + "/Cache");
    // Get singleton instance of ImageLoader
    imageLoader = ImageLoader.getInstance();
    // Create configuration for ImageLoader (all options are optional)
    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
        .denyCacheImageMultipleSizesInMemory()
        .memoryCache(new UsingFreqLimitedMemoryCache(4 * 1024 * 1024))
        //.discCache(new UnlimitedDiscCache(cacheDir))
        //.discCacheFileNameGenerator(new HashCodeFileNameGenerator())

        *//*.defaultDisplayImageOptions(
            new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_favorite_border_white_48dp)
                .resetViewBeforeLoading()
                .cacheInMemory()
                .cacheOnDisc()
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build())*//*

        .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
            //.showImageOnLoading(R.drawable.ic_favorite_border_white_48dp)
            .showImageForEmptyUri(R.drawable.error)
            .showImageOnFail(R.drawable.error)
            //.delayBeforeLoading(1000)
            .resetViewBeforeLoading(true)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .imageScaleType(Connectivity.isConnectedFast(context) ? ImageScaleType.NONE : ImageScaleType.IN_SAMPLE_INT)
            .build())

        .tasksProcessingOrder(QueueProcessingType.FIFO)
        .build();
    imageLoader.init(config);
  }

  public void displayImage(final ImageView imageView, String imageURI, final AVLoadingIndicatorView avLoadingIndicatorView) {
    if (imageView == null || imageURI == null) {
      DebugLog.d(TAG, "Either of image view or image uri is null");
      return;
    }

    imageLoader.displayImage(imageURI, imageView, new ImageLoadingListener() {

      @Override
      public void onLoadingStarted(String imageUri, View view) {
        listener.OnDownloadStarted();
        if (avLoadingIndicatorView != null)
          avLoadingIndicatorView.setVisibility(View.VISIBLE);
      }

      @Override
      public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        if (avLoadingIndicatorView != null)
          avLoadingIndicatorView.setVisibility(View.GONE);
        DebugLog.d(TAG, "onLoadingFailed: " + failReason.getType() + ", imageUri: " + imageUri);
      }

      @Override
      public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
//        imageView.setImageBitmap(getRoundedCornerBitmap(bitmap, 30));
        listener.OnDownloadFinished();
        if (avLoadingIndicatorView != null)
          avLoadingIndicatorView.setVisibility(View.GONE);
      }

      @Override
      public void onLoadingCancelled(String imageUri, View view) {
        avLoadingIndicatorView.setVisibility(View.GONE);
        DebugLog.d(TAG, "onLoadingCancelled");
      }
    });
  }

  public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    final int color = 0xff424242;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    final RectF rectF = new RectF(rect);
    final float roundPx = pixels;

    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);

    return output;
  }

  public void setOnImageDownloaderListener(OnImageDownloaderListener listener) {
    this.listener = listener;
  }*/
}